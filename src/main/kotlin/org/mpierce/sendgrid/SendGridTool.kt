package org.mpierce.sendgrid

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectReader
import com.fasterxml.jackson.databind.ObjectWriter
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.palominolabs.http.url.UrlBuilder
import org.asynchttpclient.AsyncHttpClient
import org.asynchttpclient.BoundRequestBuilder
import org.asynchttpclient.DefaultAsyncHttpClient
import org.asynchttpclient.ListenableFuture
import org.asynchttpclient.Response
import org.slf4j.LoggerFactory
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Paths
import java.util.Properties

private val logger = LoggerFactory.getLogger("org.mpierce.sendgrid.SendgridTool")

private val templateSuffixPattern = Regex(" v(\\d+)$")

fun main(args: Array<String>) {
    if (args.size != 2) {
        System.err.println("Must provide 2 args: <props file> <template id>")
        System.exit(1)
    }

    val props = Properties().apply {
        Files.newBufferedReader(Paths.get(args[0]), StandardCharsets.UTF_8).use { stream ->
            this.load(stream)
        }
    }

    val key = props.getProperty("SENDGRID_API_KEY") ?: throw RuntimeException("Missing config key")

    val templateId = args[1]

    DefaultAsyncHttpClient().use { client ->
        Tool(key, client).run {
            copyTemplate(templateId)
        }
    }
}

class Tool(private val apiKey: String, private val client: AsyncHttpClient) {

    private val reader: ObjectReader
    private val writer: ObjectWriter

    init {
        val (r, w) = ObjectMapper().run {
            registerModule(KotlinModule())
            configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

            Pair(reader(), writer())
        }

        reader = r
        writer = w
    }

    fun copyTemplate(templateId: String) {
        logger.info("Loading template $templateId")

        val template = getTemplate(templateId).let {
            if (it.statusCode != 200) {
                throw RuntimeException("Could not get template: $it")
            }

            reader
                    .forType(TemplateResponse::class.java)
                    .readValue<TemplateResponse>(it.responseBody)
        }

        val activeVersion = template.versions.firstOrNull() { it.active != 0 }
                ?: throw RuntimeException("No active version found")

        logger.info("Got template <${template.name}>")

        val newName = nameWithIncrementedVersionNumber(template.name)

        val createdTemplate = createTemplate(CreateTemplateRequest(newName, "dynamic")).let {
            if (it.statusCode != 201) {
                throw RuntimeException("Could not create new template: $it")
            }
            reader
                    .forType(TemplateResponse::class.java)
                    .readValue<TemplateResponse>(it.responseBody)
        }

        logger.info("Created template <${createdTemplate.name}> with id <${createdTemplate.id}>")

        val newVersion = activeVersion.copy(
                templateId = createdTemplate.id,
                name = nameWithIncrementedVersionNumber(activeVersion.name)
        )

        createVersion(createdTemplate.id, newVersion).let {
            if (it.statusCode != 201) {
                throw RuntimeException("Could not create version: $it")
            }
        }

        logger.info("Done!")
    }

    private fun getTemplate(templateId: String): Response {
        val url = baseUrlBuilder()
                .pathSegments("templates", templateId)
                .toUrlString()

        return client.prepareGet(url)
                .configureAndExecute()
                .get()
    }

    private fun createTemplate(req: CreateTemplateRequest): Response {
        val url = baseUrlBuilder()
                .pathSegments("templates")
                .toUrlString()

        return client.preparePost(url)
                .setBody(writer.writeValueAsString(req))
                .configureAndExecute()
                .get()
    }

    private fun createVersion(templateId: String, version: TemplateVersion): Response {
        logger.info("Creating version with name <${version.name}>")
        val url = baseUrlBuilder()
                .pathSegments("templates", templateId, "versions")
                .toUrlString()

        return client.preparePost(url)
                .setBody(writer.writeValueAsString(version))
                .configureAndExecute()
                .get()
    }

    private fun BoundRequestBuilder.configureAndExecute(): ListenableFuture<Response> {
        return setHeader("Accept", "application/json")
                .setHeader("Authorization", "Bearer $apiKey")
                .setHeader("Content-Type", "application/json")
                .execute()
    }

}

private fun nameWithIncrementedVersionNumber(name: String): String {
    val result = templateSuffixPattern.find(name)

    return if (result != null) {
        val versionNum = result.groups[1]!!
        val newNum = versionNum.value.toInt() + 1
        templateSuffixPattern.replace(name, " v$newNum")
    } else {
        "$name v2"
    }
}

private fun baseUrlBuilder() = UrlBuilder.forHost("https", "api.sendgrid.com")
        .pathSegment("v3")

class CreateTemplateRequest(
        @JsonProperty("name") val name: String,
        @JsonProperty("generation") val generation: String
)

data class TemplateResponse(
        @JsonProperty("id") val id: String,
        @JsonProperty("name") val name: String,
        @JsonProperty("versions") val versions: List<TemplateVersion>
)

data class TemplateVersion(
        @JsonProperty("template_id") val templateId: String,
        @JsonProperty("active") val active: Int,
        @JsonProperty("name") val name: String,
        @JsonProperty("html_content") val htmlContent: String,
        @JsonProperty("plain_content") val plainContent: String,
        @JsonProperty("subject") val subject: String,
        @JsonProperty("editor") val editor: String

) {
    init {
        require(active == 0 || active == 1)
        require(editor == "code" || editor == "design")
    }
}
