This tool addresses a weakness in the SendGrid Transactional Template workflow by letting you copy a template's active version to a new template, not just a new version in the template.

# Why is this needed?

SendGrid offers versions for templates, but they're pretty useless. Consider the following:

- You're sending email with dynamic template T with data provided by some server somewhere. You want to update the template, like to incorporate more dynamic data or tweak the styling.
- You make a new version and make your changes. However, as a sane person, you want to test that they work right.
- Now you're stuck! You can't send a *particular version* of a template; you can only use the active one. This means there's no way to use your new version in some non-production test capacity and let production continue to use the old active version.

That's where this tool comes in. 

- Instead of making a new version in template `T`, use this tool to copy the active version to a new template `T v2`. (The tool will generate a new name with a version in it.)
- Configure your non-production logic, whatever it might be, to use `T v2`'s id.
- Iterate on `T v2` until you're happy with it.
- When time comes to use `T v2` in production, whether that means deploying  code or not, just configure production to use `T v2` and stop using `T` entirely.
- Delete `T` once everything is using `T v2`.

# Usage

- Prepare a properties file (located anywhere you like) as follows:
 
```
SENDGRID_API_KEY=your api key here
```

- Run `./gradlew run --args 'path/to/propsfile template-id-to-copy'`. It will compile the tool and run it with the specified arguments.
- If the specified template ends in ` vX` where `X` is some integer, the new template will have that number incremented, otherwise ` v2` will be appended.
- Similarly, if the active version's name ends in ` vX`, the copy have the suffix ` vX+1`, and if not, ` v2` will be appended.
    - It appears that version names have to be unique across *all* templates, not just the template they're part of, so we can't leave the name alone.

# Notes

- The template copies created are "dynamic" templates (aka not legacy). If you need them to be legacy templates, submit a PR or issue.
- If anything goes wrong, the tool will output the HTTP response that it got back from SendGrid and stop.
    - Depending on what went wrong, it may have created some but not all of the desired resources, e.g. a template but not a version inside the template.
    - This is most likely due to a duplicate name, e.g. copying v7 to create v8 but v8 already exists. In this case, delete v8 to make room, or file an issue if you need a more fancy name conflict avoidance policy.
    - Unfortunately name clashes are tough to avoid entirely because the SendGrid API doesn't support listing dynamic templates, only legacy ones.

